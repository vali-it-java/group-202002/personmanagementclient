
async function postRegistration(registration) {
    let response = await fetch(`${API_URL}/users/register`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(registration)
    });
    return processJsonResponse(response);
}

async function postCredentials(credentials) {
    let response = await fetch(`${API_URL}/users/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(credentials)
    });
    return processJsonResponse(response);
}

async function editUserDetails(userDetails) {
    let response = await fetch(`${API_URL}/users/edituser`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        },
        body: JSON.stringify(userDetails)
    });
    return processVoidResponse(response);
}

async function fetchPersons() {
    let response = await fetch(`${API_URL}/persons/all`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    });
    return processJsonResponse(response);
}

async function postEmployee(employee) {
    let response = await fetch(`${API_URL}/persons/edit`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        },
        body: JSON.stringify(employee)
    });
    processVoidResponse(response);
}

async function postFile(file) {
    let fileUploadForm = new FormData();
    fileUploadForm.append('file', file);

    let response = await fetch(`${API_URL}/files/upload`, {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        },
        body: fileUploadForm
    });
    return processJsonResponse(response);
}

async function postEducation(education) {
    let response = await fetch(`${API_URL}/persons/educations/edit`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        },
        body: JSON.stringify(education)
    });
    processVoidResponse(response);
}

async function fetchEducationTypes() {
    let response = await fetch(`${API_URL}/persons/educationTypes`);
    return processJsonResponse(response);
}

async function processJsonResponse(response) {
    if (response.ok && response.status >= 200 && response.status < 300) {
        // Kõik on hästi!
        return await response.json();
    } else if (response.status >= 400 && response.status < 500) {
        // Autentimine ebaõnnestus!
        throw new Error('Unauthorized');
    } else {
        // Päring ebaõnnestus, aga probleem ei olnud autentimisega.
        throw new Error('Request failed', response);
    }
}

function processVoidResponse(response) {
    if (response.ok && response.status >= 200 && response.status < 300) {
        return;
    } else if (response.status >= 400 && response.status < 500) {
        // Autentimine ebaõnnestus!
        throw new Error('Unauthorized');
    } else {
        // Päring ebaõnnestus, aga probleem ei olnud autentimisega.
        throw new Error('Request failed', response);
    }
}