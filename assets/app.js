let educationTypes = [];
let persons = [];
let personId = 0;
let educationId = 0;

// Initialization after the HTML document has been loaded...
window.addEventListener('load', async () => {
    try {
        educationTypes = await fetchEducationTypes();
        persons = await fetchPersons();
        doDisplaySessionBox();
    } catch (e) {
        handleFetchError(e);
    }
    // Function calls for initial page loading activities...
    doRenderPersons(persons);
});

function doRenderPersons(filteredPersons) {
    let personsDiv = document.querySelector('#personsList');
    let personsHtml = '';

    for (let person of filteredPersons) {
        let education = educationTypes.find(type => type.id === person.educationTypeId);
        personsHtml = personsHtml + /*html*/`
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse_${person.id}"
                            aria-expanded="true" aria-controls="collapseOne">
                            ${person.firstName} ${person.lastName} / ${person.dateOfBirth} / #${person.id}
                        </button>
                    </h2>
                </div>

                <div id="collapse_${person.id}" class="collapse" aria-labelledby="headingOne"
                    data-parent="#personsList">
                    <div class="card-body">
                        <table class="table table-borderless">
                            <tbody>
                            ${
                                person.profilePhotoUrl ?
                                `<tr>
                                    <td colspan="2">
                                        <img src="${person.profilePhotoUrl}" width="300">
                                    </td>
                                </tr>` 
                                :
                                ''
                            }
                            <tr>
                                <th scope="row">Eesnimi</th>
                                <td>${person.firstName}</td>
                            </tr>
                            <tr>
                                <th scope="row">Perenimi</th>
                                <td>${person.lastName}</td>
                            </tr>
                            <tr>
                                <th scope="row">Sünniaeg</th>
                                <td>${person.dateOfBirth}</td>
                            </tr>
                            <tr>
                                <th scope="row">Haridustase</th>
                                <td>${education ? education.name : 'Haridustase puudub!'}</td>
                            </tr>
                            <tr>
                                <th scope="row">Lõpetamised</th>
                                <td>
                                    ${renderEducations(person.educations, person.id)}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <button class="btn btn-primary btn-lg" onclick="displayEmployeePopup(${person.id})">Muuda</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        `;
    }

    personsDiv.innerHTML = personsHtml;
}

function renderEducations(educations, employeeId) {
    let educationHtml = '';
    for (let education of educations) {
        educationHtml = educationHtml + /*html*/`
            <li class="list-group-item">
                <button class="btn btn-primary btn-sm" onclick="displayEducationPopup(${employeeId}, ${education.id})">Muuda</button> 
                <button class="btn btn-danger btn-sm">Kustuta</button> 
                ${education.school} (${education.graduationYear})
            </li>
        `;
    }

    educationHtml = /*html*/`
        <ul class="list-group">
            ${educationHtml}
            <li class="list-group-item">
                <button class="btn btn-primary btn-sm" onclick="displayEducationPopup(${employeeId})">Lisa lõpetamine</button>
            </li>
        </ul>
    `;
    return educationHtml;
}

/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

function displayRegisterPropup() {
    openPopup(POPUP_CONF_BLANK_600_800, 'registerTemplate');
}

async function doRegister() {
    let errors = validateRegisterForm();

    displayErrors([]);
    if (errors.length === 0) {
        let usernameInputElement = document.querySelector('#registerUsername');
        let passwordInputElement = document.querySelector('#registerPassword');

        let registration = {
            username: usernameInputElement.value,
            password: passwordInputElement.value
        };

        let credentials = {
            username: usernameInputElement.value,
            password: passwordInputElement.value
        };

        try {
            let registrationResult = await postRegistration(registration);
            if (registrationResult.errors.length === 0) {
                doProcessLogin(credentials);
            } else {
                displayErrors(registrationResult.errors);
            }
        } catch (e) {
            handleFetchError(e);
        }
    } else {
        displayErrors(errors);
    }
}

function validateRegisterForm() {
    let errors = [];

    let usernameInputElement = document.querySelector('#registerUsername');
    let passwordInputElement = document.querySelector('#registerPassword');
    let passwordInputElement2 = document.querySelector('#registerPassword2');

    if (usernameInputElement.value.length < 5 || usernameInputElement.value.length > 50) {
        errors.push('Kasutajanimi ei vasta nõuetele (min: 5, max: 50 tähemärki!)');
    }
    if (passwordInputElement.value.length < 5 || passwordInputElement.value.length > 50) {
        errors.push('Parool ei vasta nõuetele (min: 5, max: 50 tähemärki!)');
    }
    if (passwordInputElement.value !== passwordInputElement2.value) {
        errors.push('Paroolid on erinevad!');
    }

    return errors;
}

async function displayLoginPopup() {
    await openPopup(POPUP_CONF_BLANK_300_300, 'loginTemplate');
}

function doLogin() {
    let usernameInputElement = document.querySelector('#loginUsername');
    let passwordInputElement = document.querySelector('#loginPassword');

    let credentials = {
        username: usernameInputElement.value,
        password: passwordInputElement.value
    };

    doProcessLogin(credentials);
}

async function doProcessLogin(credentials) {
    try {
        let userDetails = await postCredentials(credentials);
        localStorage.setItem('VALIIT_TOKEN', userDetails.token);
        localStorage.setItem('VALIIT_USERNAME', userDetails.username);
        persons = await fetchPersons();
        await doRenderPersons(persons);
        doDisplaySessionBox();
        closePopup();
    } catch (e) {
        handleFetchError(e);
    }
}

function doDisplaySessionBox() {
    let username = localStorage.getItem('VALIIT_USERNAME');
    document.querySelector('#usernameSpan').textContent = username;
}

async function displayUserEditPopup() {
    await openPopup(POPUP_CONF_300_300, 'userEditTemplate');
}

async function doEditUser() {
    let userEditPassword = document.querySelector('#userEditPassword');
    let userEditPassword2 = document.querySelector('#userEditPassword2');

    if (userEditPassword.value.length >= 5 && userEditPassword.value === userEditPassword2.value) {
        try {
            let userDetails = {
                password: userEditPassword.value
            }
            await editUserDetails(userDetails);
            closePopup();
        } catch (e) {
            handleFetchError(e);
        }
    }
}

function doLogout() {
    localStorage.removeItem('VALIIT_TOKEN');
    localStorage.removeItem('VALIIT_USERNAME');
    displayLoginPopup();
}

// EmployeeAdd: Step 1: popup-akna avamine
async function displayEmployeePopup(employeeId) {
    await openPopup(POPUP_CONF_DEFAULT, 'employeeAddTemplate');

    let educationTypesElement = document.querySelector('#employeeEducation');

    let option = document.createElement('option');
    option.value = '';
    option.text = '--VALI--';
    educationTypesElement.appendChild(option);
    for (let type of educationTypes) {
        let option = document.createElement('option');
        option.value = type.id;
        option.text = type.name;
        educationTypesElement.appendChild(option);
    }

    if (employeeId > 0) {
        // Olmasoleva töötaja muutmine.
        let person = persons.find(p => p.id === employeeId);
        personId = person.id;

        let firstNameInputElement = document.querySelector('#employeeFirstName');
        let lastNameInputElement = document.querySelector('#employeeLastName');
        let dobInputElement = document.querySelector('#employeeDob');
        let educationTypesElement = document.querySelector('#employeeEducation');

        firstNameInputElement.value = person.firstName;
        lastNameInputElement.value = person.lastName;
        dobInputElement.value = person.dateOfBirth;
        educationTypesElement.value = person.educationTypeId;

    } else {
        // Uue töötaja lisamine.
        personId = 0;
    }
}

// EmployeeAdd: Step 2: andmete salvestamine (REST-rakendusele saatmine)
async function editEmployee() {
    let errors = validateEmployeeForm();
    if (errors.length === 0) {
        let uploadResponse = await uploadEmployeePhoto();

        let firstNameInputElement = document.querySelector('#employeeFirstName');
        let lastNameInputElement = document.querySelector('#employeeLastName');
        let dobInputElement = document.querySelector('#employeeDob');
        let educationTypesElement = document.querySelector('#employeeEducation');

        let person = {
            id: personId,
            firstName: firstNameInputElement.value,
            lastName: lastNameInputElement.value,
            dateOfBirth: dobInputElement.value,
            educationTypeId: educationTypesElement.value,
            profilePhotoUrl: uploadResponse ? uploadResponse.url : null
        };

        try {
            await postEmployee(person);
            persons = await fetchPersons();
            await doRenderPersons();
        } catch (e) {
            handleFetchError(e);
        }

        closePopup();
    }
    displayErrors(errors);
}

async function uploadEmployeePhoto() {
    let fileInputElement = document.querySelector('#employeePhoto');
    if (fileInputElement.files.length > 0) {
        let fileUploadResponse = await postFile(fileInputElement.files[0]);
        return fileUploadResponse;
    }
    return null;
}

function validateEmployeeForm() {
    let firstNameInputElement = document.querySelector('#employeeFirstName');
    let lastNameInputElement = document.querySelector('#employeeLastName');
    let dobInputElement = document.querySelector('#employeeDob');
    let educationTypesElement = document.querySelector('#employeeEducation');

    let errors = [];

    if (firstNameInputElement.value.length < 2 || firstNameInputElement.value.length > 25) {
        errors.push('Töötaja eesnimi puudu või vale pikkusega!');
    }

    if (lastNameInputElement.value.length < 2 || lastNameInputElement.value.length > 25) {
        errors.push('Töötaja perenimi puudu või vale pikkusega!');
    }

    if (dobInputElement.value.length < 1) {
        errors.push('Töötaja sünniaeg määramata!');
    } else if (new Date(dobInputElement.value).getTime() > new Date().getTime()) {
        errors.push('Töötaja peab oleme sündinud enne, kui me ta tööle võtame!');
    }

    if (educationTypesElement.value.length < 1) {
        errors.push('Haridustase määramata!');
    }

    return errors;
}

async function displayEducationPopup(employeeId, personEducationId = 0) {
    await openPopup(POPUP_CONF_DEFAULT, 'educationTemplate');

    personId = employeeId;
    educationId = personEducationId;

    if (personEducationId > 0) {
        let schoolInputElement = document.querySelector('#employeeSchool');
        let graduationYearInputElement = document.querySelector('#employeeSchoolGraduationYear');

        let person = persons.find(p => p.id === employeeId);
        let education = person.educations.find(e => e.id === personEducationId);

        schoolInputElement.value = education.school;
        graduationYearInputElement.value = education.graduationYear
    }
}

async function editEducation() {
    let schoolInputElement = document.querySelector('#employeeSchool');
    let graduationYearInputElement = document.querySelector('#employeeSchoolGraduationYear');

    let education = {
        id: educationId,
        school: schoolInputElement.value,
        graduationYear: graduationYearInputElement.value,
        personId: personId
    };

    try {
        await postEducation(education);
        persons = await fetchPersons();
        await doRenderPersons();
    } catch (e) {
        handleFetchError(e);
    }

    closePopup();
}

function displayErrors(errors) {
    let errorDivElement = document.querySelector('#errorBox');
    if (errors.length > 0) {
        let errorsHtml = '<h4>Ilmnesid probleemid</h4><hr>';
        for (let error of errors) {
            errorsHtml = errorsHtml + /*html*/`<p>${error}</p>`;
        }
        errorDivElement.innerHTML = errorsHtml;
        errorDivElement.style.display = 'block';
    } else {
        errorDivElement.style.display = 'none';
    }
}

function handleFetchError(e) {
    if (e.message === 'Unauthorized') {
        displayLoginPopup();
    } else {
        console.log(e);
    }
}

function doSearch(event) {
    console.log(event.target.value);
    let searchString = event.target.value.toLowerCase();
    let filteredPersons = persons.filter(p => {
        let firstNameMatch = p.firstName.toLowerCase().includes(searchString);
        let lastNameMatch = p.lastName.toLowerCase().includes(searchString);

        let educationType = educationTypes.find(et => et.id === p.educationTypeId);
        let educationTypeMatch = educationType && educationType.name.toLowerCase().includes(searchString);

        let schoolMatch = p.educations.some(ed => ed.school.toLowerCase().includes(searchString));

        return firstNameMatch || lastNameMatch || educationTypeMatch || schoolMatch;
    });
    doRenderPersons(filteredPersons);
}